# Readme

Electrical symbols for Inkscape This set of symbols can be used to make professional circuit schematics in Inkscape.

Procedure for making it the default document in inkscape:

    Open the .svg file (downloadable from the repository) in Inkscape.
    Go to File -> Save Template
    Save the file with an appropriate name, check the "set as default template" checkbox, and save the file.
    Reopen Inkscape and check if the template is opened as the default file.

Prof. L Umanand (Department of Electronic Systems Engineering, Indian Institute of Science) created the original template file in early 2000s. This version of the file is a minor update of the original template.

Most of these changes were made by members of the Power Electronics Group (PEG-DESE).
